#include "ClientThread.h"
#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "HTTPError.h"
#include "HTML.h"
#include "Util.h"

namespace {
/// Gets next line, excluding, but consuming newlines
	string GetNextLine(Socket &s) {
		string result;

		while(s.HasData()) {
			char c;
			int len = s.Recv(&c, 1);

			if(len == 0 || c == '\n')
				break;
			if(c == '\r')
				continue;

			result += c;
		}

		return result;
	}

	HTTPRequest ParseRequest(Socket &client) {
		HTTPRequest request;

		while(client) {
			string line = GetNextLine(client);
			if(line == "")
				break;
			request.ParseLine(line);
		}

		return request;
	}

}

ClientThread::ClientThread(const Socket &s, ContentManager &c) : socket(s), content(c) {
	socket.ShouldClose();
}

void ClientThread::Process() {
	HTTPResponse response;

	try {
		if(!socket.HasData(1))
			throw RequestTimeout;

		HTTPRequest req = ParseRequest(socket);

		if(!req.Valid())
			throw BadRequest;

		if(req.method == "GET") {
			response.SetOK();
			content.GetResource(req.path, response);
		} else {
			throw NotImplemented;
		}
	} catch (const HTTPError &error) {
		response.code   = error.code;
		response.reason = error.reason;
		if(error.showErrorPage) {
			HTMLPage errorPage(stringf("%d ", response.code) + response.reason);
			response << errorPage.Generate();
		}
	}

	socket.Send(response.Generate());

	//--- destroy this malloc'd thread subclass when done processing
	delete this;
}

