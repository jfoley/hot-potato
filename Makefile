CXX      := g++
CXXFLAGS := -Wall -Werror -O2
LDFLAGS  := -lpthread
OBJS     := main.o Address.o ClientThread.o ContentManager.o FileSystem.o HTML.o HTTPRequest.o HTTPResponse.o Socket.o Thread.o Util.o 
OUTPUT   := xmain

.PHONY: clean all test

# define a rule for making an o from a .cpp
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< 

#define a rule for linking together OBJS into OUTPUT
$(OUTPUT): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^

all: $(OUTPUT)

test: all
	./$(OUTPUT)

clean:
	@rm -rf $(TARGETS) $(OBJS) $(OUTPUT)

