#include "FileSystem.h"
#include "Util.h"
#include "Mutex.h"

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdio>


namespace {
	const string PathSepStr = "/";
	const char PathSepChar = PathSepStr[0];
	Mutex lsMutex;

	//--- this is private because anyone else can get it by using "." or ""
	string currentDirectory() {
		string result;
		result.resize(1024);

		if(getcwd(&result[0], result.size()) == 0)
			return "";

		return string(result.c_str());
	}
}

string Which(string name) {
	string path = getenv("PATH");
	// string is PATH=a:b:c:d:...
	path = splitAfter(path, '=');

	vector<string> options = split(path, ':');

	for(size_t i=0; i<options.size(); i++) {
		Path current(options[i]);

		//--- find name in files
		vector<Path> files = current.List();
		for(size_t j=0; j<files.size(); j++) {
			if(files[j].Name() == name) {
				return files[j];
			}
		}
	}

	return "";
}

bool MakeDir(string path) {
	Path p(path);

	//--- return success if directory already exists
	if(p.Exists())
		return p.Directory();

	if(mkdir(p.c_str(), 0777) == -1) {
		//perror("mkdir");
		return false;
	}

	return true;
}

bool Remove(string path) {
	Path p(path);

	if(remove(p.c_str()) == -1) {
		//perror("rmdir");
		return false;
	}

	return true;
}

bool RemoveAll(string path) {
	const Path p(path);

	if(!p.Exists() || !p.Directory())
		return false;

	vector<Path> contents = p.List();
	for(size_t i=0; i<contents.size(); i++) {
		const Path &cur = contents[i];

		if(cur.Directory() && !cur.Empty()) {
			//--- recursively remove contents
			if(!RemoveAll(cur))
				return false;
		} else {
			//--- remove this file
			if(!Remove(cur))
				return false;
		}
	}
	
	//--- remove directory itself
	if(!Remove(p))
		return false;

	return true;
}

string SimplifyPath(string path) {
	string result;

	result += PathSepChar;

	//--- if not absolute, make it so
	if(!startsWith(path, PathSepStr)) {
		path = currentDirectory() + PathSepChar + path;
	}

	vector<string> input = split(path, PathSepChar);
	vector<string> output;
	output.reserve(input.size());

	for(size_t i=0; i<input.size(); i++) {
		const string &cur = input[i];
		
		//--- don't preserve needless dots
		if(cur == "." || cur == "")
			continue;

		if(cur == "..") {
			if(output.size())
				output.pop_back();
			continue;
		}

		output.push_back(cur);
	}
	
	result += join(output, PathSepChar);

	return string(result);
}

bool MakeDirR(string path) {
	vector<string> pieces = split(path, PathSepChar);
	assert(pieces.size() == 3);

	// iterate through building the path for /a/b/c/d
	// /a, /a/b, /a/b/c ...
	for(size_t i=0; i<pieces.size(); i++) {
		//--- join path so far
		string cur;
		for(size_t j=0; j<=i; j++) {
			if(j != 0) cur += '/';
			cur += pieces[j];
		}

		//--- attempt to create if it doesn't exist
		Path dir(cur);
		if(dir.Exists()) {
			if(!dir.Directory())
				return false;
			continue;
		}
		
		//--- create if doesn't exist
		if(!MakeDir(dir)) return false;
	}
	return true;
}

Path::Path(string p) {
	Set(p);
}

void Path::Set(string p) {
	path = SimplifyPath(p);
	Refresh();
}

void Path::Refresh() {
	exists = false;
	file = false;
	directory = false;
	size = 0;

	struct stat file_stats;

	//--- if stat fails, we consider the file as not existing
	if(stat(path.c_str(), &file_stats) == -1)
		return;

	exists = true;

	file = S_ISREG(file_stats.st_mode);
	directory = S_ISDIR(file_stats.st_mode);
	size = file_stats.st_size;
}

string Path::Name() const {
	return splitAfterLast(path, PathSepChar);
}

Path Path::UpDir() const {
	string result = path;

	//--- backtrack over file
	if(File()) {
		result = splitBeforeLast(result, PathSepChar);
	}
	
	//--- backtrack one dir
	return Path(result + PathSepChar + "..");
}

Path Path::ChDir(string rel) const {
	//--- make a new path if given absolute
	if(startsWith(rel, PathSepStr))
		return Path(rel);
	else
		return Path(path + PathSepChar + rel);
}

bool Path::Hidden() const {
	string n = Name();
	return startsWith(n, ".");
}

vector<Path> Path::List() const {
	vector<Path> results;
	
	if(!Directory())
		return results;
	
	DIR *dir = 0;
	dirent *entry = 0;

	ScopeLock lock(lsMutex);

	dir = opendir(path.c_str());

	if(!dir)
		return results;

	do {
		entry = readdir(dir);
		if(entry) {
			string name = entry->d_name;
			
			if(name == "." || name == "..")
				continue; //--- skip special elements

			results.push_back(string(path) + PathSepChar + string(name));
		}
	} while(entry);

	closedir(dir);

	return results;
}

bool Path::Empty() const {
	return List().size() == 0;
}

bool Path::Contains(const string &p) const {
	Path other(p);
	return startsWith(other.path, path);
}

