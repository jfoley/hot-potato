#ifndef _CONTENTMANAGER_H
#define _CONTENTMANAGER_H

#include "FileSystem.h"
#include "HTTPResponse.h"

class ContentManager {
	public:
		ContentManager(string dirToHost=".");
		void GetResource(const string &, HTTPResponse &);
	
	private:
		void OutputDirectory(const string &, HTTPResponse &);
		void OutputCode(const string &, HTTPResponse &);
		bool HasResource(const string &);
		Path ResolvePath(const string &);
		Path root;
};


#endif

