#ifndef _THREAD_H
#define _THREAD_H

#include <pthread.h>

class Thread {
	public:
		virtual ~Thread() { }
		void Start();
		virtual void Process() = 0;
	private:
		pthread_t self;
};



#endif

