#include "Socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <cstdio>

Socket::Socket() {
	autoclose = false;
	handle = -1;
	type = ErrorSocket;
}

Socket::Socket(const Socket &rhs) {
	handle = rhs.handle;
	type   = rhs.type;
	autoclose = false;
}

Socket::Socket(int newHandle, SocketType newType) {
	handle = newHandle;
	type   = newType;
	autoclose = false;
}

bool Socket::OpenStream() {
	Close();
	type = StreamSocket;
	handle = socket(AF_INET, SOCK_STREAM, 0);
	return Valid();
}

bool Socket::OpenStreamServer(u16 port, int connections) {
	return OpenStream() && Bind(port) && Listen(connections);
}

bool Socket::OpenStreamClient(const Address &target) {
	return OpenStream() && Connect(target);
}

void Socket::Close() {
	if(Valid()) {
		close(handle);
		handle = -1;
	}
}

bool Socket::Valid() const {
	return handle >= 0;
}

Address Socket::GetAddress() const {
	if(!Valid()) return Address();

	sockaddr_storage buf = {0};
	socklen_t bufSize = sizeof(buf);

	if(getsockname(handle, (sockaddr*) &buf, &bufSize) == -1) {
		perror("getsockname");
		return Address();
	}

	return Address(&buf, bufSize);
}

bool Socket::HasData(int timeout) {
	if(!Valid()) return false;

	fd_set files;
	struct timeval time;
	
	time.tv_sec = timeout;
	time.tv_usec = 0;
	if(!timeout) {
		time.tv_usec = 2;
	}

	FD_ZERO(&files);
	FD_SET(handle, &files);

	int rc = select(32, &files, 0, 0, &time);
	if(rc == -1) {
		perror("select");
		return false;
	}

	return rc != 0;
}

bool Socket::Bind(u16 port) {
	sockaddr_in address = {0};
	address.sin_family      = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);

	//--- allow reusing addresses
	const int Yes=1;
	if (setsockopt(handle, SOL_SOCKET, SO_REUSEADDR, &Yes, sizeof(Yes)) == -1) {
		perror("setsockopt");
		return false;
	} 

	if (bind(handle, (const sockaddr*) &address, sizeof(address)) == -1) {
		perror("bind");
		return false;
	}

	return true;
}

bool Socket::Connect(const Address &target) {
	if(!target || !Valid())
		return false;

	if(connect(handle, (const sockaddr*) target.Ptr(), target.Size()) == -1) {
		perror("connect");
		return false;
	}

	return true;
}

bool Socket::Listen(int connections) {
	if(!Valid()) return false;

	if(listen(handle, connections) == -1) {
		perror("listen");
		return false;
	}

	return true;
}

Socket Socket::Accept(Address *clientAddr) {
	if(!Valid()) { return Socket(); }

	sockaddr_storage buf = {0};
	socklen_t bufSize = sizeof(buf);
	
	int client = accept(handle, (sockaddr*) &buf, &bufSize);
	if(client == -1) {
		perror("accept");
		return Socket();
	}

	if(clientAddr != 0) {
		*clientAddr = Address(&buf, bufSize);
	}

	return Socket(client, StreamSocket);
}

bool Socket::Send(const void *msg, size_t len) {
	if(!Valid()) return false;
	if(len) assert(msg);

	size_t start = 0;

	while(start < len && Valid()) { 
		ssize_t sent = send(handle, ((u8*)msg) + start, len-start, 0);
		if(sent == -1) return false;
		
		start += sent;
	}

	assert(Valid());

	return true;
}

u32 Socket::Recv(void *msg, size_t len) {
	if(!Valid()) return -1;
	if(len) assert(msg);

	ssize_t found = recv(handle, msg, len, 0);

	if(found < 0) {
		Close();
		found = 0;
	}

	return found;
}

string Socket::RecvAll() {
	string result;

	while(HasData()) {
		string buf;
		buf.resize(1024);

		int len = Recv(&buf[0], buf.size());
		
		//--- client has hung up, close socket
		if(len <= 0) {
			Close();
			break;
		}

		buf.resize((size_t) len);
		result += buf;
	}

	return result;
}



