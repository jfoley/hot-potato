#include "ContentManager.h"
#include "Util.h"
#include "FileSystem.h"
#include <fstream>
using std::fstream;

#include "HTML.h"
#include "HTTPError.h"

ContentManager::ContentManager(string hostedDirectory) {
	root = hostedDirectory;
}

Path ContentManager::ResolvePath(const string& resource) {
	Path result(string(root) + "/." + string(resource));
	return result;
}

bool ContentManager::HasResource(const string& resource) {
	Path res = ResolvePath(resource);

	//--- they requested ".."
	if(!root.Contains(res))
		return false;

	return res.Exists();
}

void ContentManager::OutputDirectory(const string &resource, HTTPResponse &response) {
	response.SetIsHTML();

	Path res = ResolvePath(resource);
	HTMLPage listOutput("Contents of " + resource);

	vector<Path> contents = res.List();

	listOutput.body.OpenTag("table", "padding=\"10em\"");
	listOutput.body.OpenTag("tr");
	listOutput.body << HTML("th", "Name") << HTML("th", "Size (bytes)") << HTML("th", "Access");
	listOutput.body.CloseTag("tr");

	for(u32 i=0; i<contents.size(); i++) {
		if(contents[i].Hidden() || endsWith(contents[i].Name(), ".o"))
			continue;

		listOutput.body.OpenTag("tr");
		
		const bool dir = contents[i].Directory();
		const string name = contents[i].Name() + (dir?"/":"");

		string linkPath = name;
		if(resource != "/") {
			linkPath = resource + '/' + name;
		}

		// Name column
		listOutput.body.Tag("td", name);

		// Size Column
		if(dir) {
			listOutput.body.Tag("td", "--");
		} else {
			listOutput.body.Tag("td", stringf("%d", contents[i].Size()));
		}

		listOutput.body.OpenTag("td");
		if(dir) {
			listOutput.body.Tag("a", "Browse", "href=\""+linkPath+"\"");
		} else {
			listOutput.body << HTML("a", "View", "href=\"" + linkPath + "?code\"");
			listOutput.body << "&nbsp;";
			listOutput.body << HTML("a", "Download", "href=\"" + linkPath + "\"");
		}
		listOutput.body.CloseLastTag();

		listOutput.body.CloseTag("tr");
	}
	response << listOutput.Generate();
}

void ContentManager::OutputCode(const string &resource, HTTPResponse &response) {
	response.SetIsHTML();
	Path res = ResolvePath(resource);

	string fileName = res.Name();
	string extension = splitAfterLast(fileName, '.');

	HTMLPage codePage(fileName);

	codePage.StyleSheet("/styles/solarized_light.css");
	codePage.ScriptLink("/js/highlight.pack.js");
	codePage.head << HTML("script", "hljs.initHighlightingOnLoad(); hljs.tabReplace = '  ';");
	
	//--- woot
	HTML codeExample;
	codeExample.OpenTags("table tr td pre");
	codeExample.OpenTag("code", "class=\""+extension+"\"");
	//codeExample << "#include &lt;cstdio&gt;\n\nint main(int argc, char **argv) {\n\tprintf(\"Hello, World!\\n\");\n\treturn 0;\n}\n\n";
	fstream file;

	file.open(res.c_str(), fstream::in);
	while(file.good()) {
		int x = file.get();
		if(x == -1)
			break;

		char c = x;
		switch(c) {
			case '<':
				codeExample << "&lt;";
				break;
			case '>':
				codeExample << "&gt;";
				break;
			case '&':
				codeExample << "&amp;";
				break;
			default:
				codeExample << stringf("%c",c);
				break;
		}
	}

	codePage.body << codeExample;
	codePage.body.CloseAllTags();
	codePage.body.SelfClosedTag("hr");

	response << codePage.Generate();
}

void ContentManager::GetResource(const string &resource, HTTPResponse &response) {
	string res_path = splitBefore(resource, '?');
	string get_opts = splitAfter(resource, '?');

	Path res = ResolvePath(res_path);
	if(!root.Contains(res)) throw Forbidden;
	if(!res.Exists()) throw NotFound;

	if(res.Directory()) {
		OutputDirectory(res_path, response);
	} else {
		
		if(get_opts == "code") {
			return OutputCode(res_path, response);
		}

		fstream file;

		file.open(res.c_str(), fstream::in);
		while(file.good()) {
			int x = file.get();
			if(x == -1)
				break;
			
			response << char(x);
		}
	}
}

