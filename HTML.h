#ifndef _HTML_H
#define _HTML_H

#include "types.h"
#include <deque>

class HTML {
	public:
		HTML() { }
		HTML(string type, string contents, string opts="") { Tag(type, contents, opts); }
		
		void OpenTags(string types);
		void Tag(string type, string contents, string opts="");
		void OpenTag(string type, string opts="");
		void NonClosedTag(string type, string opts="");
		void SelfClosedTag(string type, string opts="");
		void CloseLastTag();
		void CloseTag(string name);
		void CloseAllTags();
		
		void Insert(const HTML &other);
		//--- C++ had some good ideas
		HTML& operator<<(const HTML& other) { Insert(other); return *this; }
		HTML& operator<<(const string& other) { contents << other; return *this; }

		string Generate() const;
		operator string() const { return Generate(); }
	private:
		std::deque<string> tagStack;
		stringstream contents;
};

class HTMLPage {
	public:
		HTMLPage(string title="Untitled");
		void ScriptLink(string path);
		void StyleSheet(string path);
		void Meta(string key, string value);

		string Generate() const;
		
		HTML head;
		HTML body;
};




#endif

