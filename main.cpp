#include "types.h"
#include "ContentManager.h"
#include "ClientThread.h"
#include "Util.h"

int main(int argc, char **argv) {
	Socket server;

	if(argc > 2) {
		cerr << "\n\n" << argv[0] << " *** error: expects 0 or 1 argument. Port=1234\n";
		return -1;
	}
	u16 port = 1234;
	
	if(argc == 2)
		port = toInt(argv[1], port);
	
	if(!server.OpenStreamServer(port, 10)) {
		cerr << "Could not start HTTP server on port " << port << "\n";
		return -1;
	}

	cout << "Started Hot Potato HTTP Server on Port " << port << "\n";

	//--- start a content manager offering up this directory
	ContentManager content(".");

	while(server) {
		ClientThread *thread = new ClientThread(server.Accept(), content);
		thread->Start();
	}

	server.ShouldClose();

	cout << "Hello, World!\n";
	return 0;
}

