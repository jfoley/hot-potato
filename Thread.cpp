#include "Thread.h"
#include "types.h"
#include <sched.h>

namespace {
	struct Argument {
		volatile bool started;
		Thread* instance;
	};

	void* callback(void* ptr) {
		Argument *arg = (Argument*) ptr;

		Thread* thread = arg->instance;
		// notify Thread::Start() that we've been scheduled
		arg->started = true;
		thread->Process();
		return 0;
	}
};

void Thread::Start() {
	Argument arg;
	arg.started = false;
	arg.instance = this;

	pthread_create(&self, 0, callback, &arg);

	while(!arg.started) {
		sched_yield();
	}
}


