#ifndef _HTTPREQUEST_H
#define _HTTPREQUEST_H

#include "types.h"

class HTTPRequest {
	public:
		HTTPRequest();
		bool Valid() const; 
		
		void ParseLine(const string&);

		string method;
		string path;
	private:
		void RequestLine(const string&);
		void RequestHeader(const string&);

		bool badVersion;
};





#endif

