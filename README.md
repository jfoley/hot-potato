# README
------------------------------------------------------------
  Hot Potato (HTTP Server)
	Final Project
  Network Design
  Spring 2012

# AUTHOR:
------------------------------------------------------------
  John Foley

# ABOUT:
------------------------------------------------------------
  Simple HTTP server implemented in C++

# COMPILING:
------------------------------------------------------------
  $ make
	Depends on pthreads and g++

# RUNNING:
------------------------------------------------------------
	$ ./xmain portNumber
	 by default portNumber = 1234
	eg.
	$ ./xmain 443
	$ ./xmain 80
	$ ./xmain 8080

# TESTING:
------------------------------------------------------------
	$ wget http://localhost:portNumber
	 by default portNumber = 1234
	$ firefox http://localhost:1234


