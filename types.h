#ifndef _TYPES_H
#define _TYPES_H

#include <string>
using std::string;
#include <vector>
using std::vector;

#include <iostream>
#include <sstream>
using std::cout;
using std::cin;
using std::cerr;
using std::ostream;
using std::stringstream;

#include <stdint.h>
typedef uint8_t   u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t    i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))

//--- clip x to [a,b]
#define CLAMP(x,a,b) MIN(b,MAX(x,a))

#define show(var) \
	do { std::cout << __FILE__ << ":" << __LINE__ << ": " << #var << " = " << (var) << "\n"; std::cout.flush(); } while (0)

#include <cassert>
#include <cstdlib>
#include <cstdio>

#endif

