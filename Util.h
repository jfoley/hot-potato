#ifndef _UTIL_H
#define _UTIL_H

#include "types.h"

string stringf (const char * fmt, ...);
string toLower (const string &);
string toUpper (const string &);
bool   isSpaces (const string &);
string compactSpaces (const string &);
char   reverseElement (const string &src, size_t i);
bool   startsWith (const string &src, const string &prefix);
bool   endsWith (const string &src, const string &suffix);
int    toInt (const string &n, int fallback);


// The following functions are templated so they will accept either characters or strings for delimiters

template <class T>
bool contains (const string &haystack, const T& needle) {
	return haystack.find(needle) != string::npos;
}

template <class T>
vector<string> split (const string &src, const T& delimiter) {
	vector<string> result;
	string cur = src;
	
	while(contains(cur, delimiter)) {
		size_t pos = cur.find(delimiter);
		result.push_back(cur.substr(0,pos));
		cur = cur.substr(pos+1);
	}

	result.push_back(cur);
	return result;
}

template <class T>
string join (const vector<string> &src, const T& delimiter) {
	string result;

	for(size_t i=0; i<src.size(); i++) {
		if(i != 0) {
			result += delimiter;
		}
		result += src[i];
	}

	return result;
}

template <class T>
string splitAfter(const string &src, const T& delimiter) {
	size_t pos = src.find(delimiter);
	return (pos == string::npos) ? src : src.substr(pos+1);
}

template <class T>
string splitAfterLast(const string &src, const T& delimiter) {
	size_t pos = src.rfind(delimiter);
	return (pos == string::npos) ? src : src.substr(pos+1);
}

template <class T>
string splitBeforeLast(const string &src, const T& delimiter) {
	size_t pos = src.rfind(delimiter);
	return (pos == string::npos) ? src : src.substr(0,pos);
}

template <class T>
string splitBefore(const string &src, const T& delimiter) {
	size_t pos = src.find(delimiter);
	return (pos == string::npos) ? src : src.substr(0,pos);
}

#endif

