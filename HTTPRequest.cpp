#include "HTTPRequest.h"
#include "Util.h"

HTTPRequest::HTTPRequest() {
	badVersion = false;
}

bool HTTPRequest::Valid() const {
	return method != ""
		&& !badVersion;
}

void HTTPRequest::ParseLine(const string &line) {
	//show(line);
	if(method == "") {
		RequestLine(line);
	} else {
		RequestHeader(line);
	}
}

void HTTPRequest::RequestLine(const string &line) {
	vector<string> tokens = split(line, ' ');
	
	if(tokens.size() != 3) {
		badVersion = true;
		return;
	}

	method = toUpper(tokens[0]);
	path   = tokens[1];

	string protocol = tokens[2];
	string proto_name = splitBefore(protocol, '/');
	string proto_version = splitAfter(protocol, '/');
	
	if(proto_name != "HTTP" && (proto_version != "1.1" || proto_version != "1.0")) {
		badVersion = true;
	}

	cout << "{method:\""<<method << "\", path:\"" << path << "\"}\n";
}

void HTTPRequest::RequestHeader(const string &line) {
	if(!contains(line, ':')) {
		cerr << "Strangely formatted RequestHeader \"" << line << "\"\n";
	} else {
		//cout << line << "\n";
	}
}


