#ifndef _HTTPERROR_H
#define _HTTPERROR_H

#include "types.h"

struct HTTPError {
	HTTPError(int c, string r, bool p=true) : code(c), reason(r), showErrorPage(p) { }
	
	int code;
	string reason;
	bool showErrorPage;
};

const HTTPError NoContent(204, "No Content", false);
const HTTPError BadRequest(400, "Bad Request");
const HTTPError Forbidden(403, "Forbidden");
const HTTPError NotFound(404, "Not Found");
const HTTPError RequestTimeout(408, "Request Timeout");
const HTTPError InternalErr(500, "Internal Server Error");
const HTTPError NotImplemented(501, "Not Implemented");



#endif

