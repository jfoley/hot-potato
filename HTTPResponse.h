#ifndef _HTTP_RESPONSE_H
#define _HTTP_RESPONSE_H

#include "HTTPRequest.h"

class HTTPResponse {
	public:
		HTTPResponse();
		void SetOK() { code = 200; reason = "OK"; }
		void SetIsHTML() { AddHeader("Content-Type", "text/html; charset=utf-8"); }

		template <typename T>
		void AddHeader(const string &key, const T &value) {
			stringstream conv;
			conv << value;
			PushHeader(key, conv.str());
		}
		
		void AddContent(string data);
		string Generate() const;
		
		int code;
		string reason;
	private:
		void PushHeader(const string &key, const string &value);
		string headers;
		string content;

};

/// IOStream style operator for appending to a HTTP response
template <class T>
HTTPResponse& operator<<(HTTPResponse& out, const T& rhs) {
	stringstream stream;
	stream << rhs;
	out.AddContent(stream.str());
	return out;
}


#endif

