#include "HTTPResponse.h"

HTTPResponse::HTTPResponse() {
	code = 501;
	reason = "Internal Server Error";
}

void HTTPResponse::PushHeader(const string &key, const string &value) {
	headers += key +": " + value + "\r\n";
}


string HTTPResponse::Generate() const {
	stringstream result;
	result << "HTTP/1.1 " << code << " " << reason << "\r\n";
	result << headers;
	result << "Content-Length: " << content.size() << "\r\n";
	// terminate with empty line followed by content
	result << "\r\n";
	result << content;
	return result.str();
}

void HTTPResponse::AddContent(string data) {
	content += data;
}



