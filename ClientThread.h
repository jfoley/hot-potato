#ifndef _CLIENTTHREAD_H
#define _CLIENTTHREAD_H

#include "Thread.h"
#include "Socket.h"
#include "ContentManager.h"

class ClientThread : public Thread {
	public:
		ClientThread(const Socket &s, ContentManager &content);
		virtual ~ClientThread() { }
		virtual void Process();
	private:
		Socket socket;
		ContentManager &content;
};

#endif

