#include "HTML.h"
#include "Util.h"

void HTML::Tag(string type, string data, string opts) {
	OpenTag(type, opts);
	contents << data;
	CloseLastTag();
}

void HTML::OpenTags(string tags) {
	vector<string> tagList = split(tags, ' ');
	for(u32 i=0; i<tagList.size(); i++) {
		OpenTag(tagList[i]);
	}
}

void HTML::NonClosedTag(string type, string opts) {
	contents << "<" << type;
	if(opts != "") {
		contents << " " + opts;
	}
	contents << ">";
}

void HTML::SelfClosedTag(string type, string opts) {
	contents << "<" << type;
	if(opts != "") {
		contents << " " + opts;
	}
	contents << " />";
}

void HTML::OpenTag(string type, string opts) {
	NonClosedTag(type, opts);
	tagStack.push_back(type);
}

void HTML::CloseLastTag() {
	contents << "</" << tagStack.back() << ">";
	tagStack.pop_back();
}

void HTML::CloseTag(string name) {
	while(!tagStack.empty()) {
		string tag = tagStack.back();
		CloseLastTag();
		
		if(tag == name)
			break;
	}
}

void HTML::CloseAllTags() {
	while(!tagStack.empty()) {
		CloseLastTag();
	}
}

void HTML::Insert(const HTML &other) {
	contents << other.contents.str();
	
	// append possibly filled stack from other tree
	for(u32 i=0; i<other.tagStack.size(); i++) {
		tagStack.push_back(other.tagStack[i]);
	}
}

string HTML::Generate() const {
	assert(tagStack.empty());
	return contents.str();
}

HTMLPage::HTMLPage(string name) {
	head.Tag("title", name);
	Meta("author", "jfoley");
	body.Tag("h1", name);
	body.SelfClosedTag("hr");
}

void HTMLPage::StyleSheet(string path) {
	head.NonClosedTag("link", "href=\""+path+"\" rel=\"stylesheet\" type=\"text/css\"");
}

void HTMLPage::ScriptLink(string path) {
	head.Tag("script", "", "src=\""+path+"\"");
}

void HTMLPage::Meta(string key, string value) {
	stringstream opts;
	opts << "name=\"" << key << "\" content=\"" << value << "\"";
	head.OpenTag("meta", opts.str());
}

string HTMLPage::Generate() const {
	HTML page;
	page.OpenTag("html");
	page.OpenTag("head");
	page.Insert(head);
	page.CloseTag("head");
	page.OpenTag("body");
	page.Insert(body);
	page.CloseAllTags();

	return page.Generate();
}

